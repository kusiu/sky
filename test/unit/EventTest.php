<?php

use \SSDMTechTest\Event;
use \SSDMTechTest\EventType;
use \SSDMTechTest\Sport;
use \SSDMTechTest\EventStorage;

class EventTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test case to verify game of football
     */
    public function testGetSport()
    {
        $event = new Event();
        $event->setSport(Sport::FOOTBALL);
        $this->assertEquals('football', $event->getSport());
    }

    /**
     * Test default event type
     */
    public function testDefaultEventType()
    {
        $event = new Event();
        $event->setSport(Sport::FOOTBALL);

        $this->assertEquals($event->getEventType(), 'kickoff');
    }

    /**
     * Event not processed
     */
    public function testEventNotProcessed()
    {
        $event = new Event();
        $event->setSport(Sport::FOOTBALL);
        $event->setEventType('');

        $eventStorage = new EventStorage();

        $this->assertFalse($eventStorage->store($event));
        $this->assertEmpty($eventStorage->getEvents());
    }

    /**
     * Adding events for the football
     */
    public function testAddEvents()
    {
        $event = new Event();
        $event->setSport(Sport::FOOTBALL);

        $event2 = new Event();
        $event2->setSport(Sport::FOOTBALL);
        $event2->setEventType(EventType::TYPE_CORNER);

        $eventStorage = new EventStorage();

        $this->assertTrue($eventStorage->store($event));
        $this->assertTrue($eventStorage->store($event2));
        $this->assertEquals($event2->getEventType(), 'corner');
        $this->assertCount(2, $eventStorage->getEvents());
    }

}