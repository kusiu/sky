<?php

namespace SSDMTechTest;


class Event implements EventInterface
{
    /**
     * @var string sport
     */
    private $sport;

    /**
     * @var string eventType
     */
    private $eventType;

    function __construct()
    {
        $this->setEventType(EventType::TYPE_KICKOFF);
    }

    /**
     * Set sport
     *
     * @param string $sport
     * @return void
     */
    public function setSport($sport)
    {
        $this->sport = $sport;
    }

    /**
     * Get sport
     *
     * @return string
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set event type
     *
     * @param string $eventType
     * @return void
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * Get event type
     *
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

}