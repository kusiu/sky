<?php

namespace SSDMTechTest;


class EventStorage implements EventStorageInterface
{

    /**
     * @var array events
     */
    private $events = [];

    /**
     * Stores an event
     *
     * @param  EventInterface $event
     * @return boolean
     */
    public function store(EventInterface $event)
    {
        if ($event->getEventType()) {
            $this->events[] = $event;
            return true;
        }

        return false;
    }

    /**
     * Get events
     *
     * @return array
     */
    public function getEvents()
    {
        return $this->events;
    }
}