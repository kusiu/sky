<?php

namespace SSDMTechTest;


class EventType
{
    const TYPE_KICKOFF = 'kickoff';
    const TYPE_HALFTIME = 'halftime';
    const TYPE_FULLTIME = 'fulltime';
    const TYPE_EXTRATIME = 'extratime';
    const TYPE_YELLOWCACD = 'yellowcard';
    const TYPE_REDCARD = 'redcard';
    const TYPE_PENALTY = 'penalty';
    const TYPE_FREEKICK = 'freekick';
    const TYPE_CORNER = 'corner';
    const TYPE_GOAL = 'goal';
}